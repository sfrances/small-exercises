// Introduction to Histograms in C++

#include <iostream>
#include <TH1F.h>
#include <TRandom3.h>

// standard c++ libs for gaussians

void generate_z_peak_standard_cpp() {
    // Generating Z boson mass peak using standard C++ random number generation
    const int nEvents = 10000;
    double mass_peak_mean = 91.2;  // Mean mass of Z boson
    double mass_peak_width = 2.5; // Width of the mass peak

    TH1F *histogram = new TH1F("histogram", "Z Boson Mass Peak", 100, 80, 100);

    // Standard C++ random number generator
    std::default_random_engine generator;
    std::normal_distribution<double> distribution(mass_peak_mean, mass_peak_width);

    for (int i = 0; i < nEvents; ++i) {
        double mass = distribution(generator);
        histogram->Fill(mass);
    }

    // Draw histogram
    histogram->Draw();
}

// using TRandom

void generate_z_peak_root_cpp() {
    // Generating Z boson mass peak using ROOT's TRandom3
    const int nEvents = 10000;
    double mass_peak_mean = 91.2;  // Mean mass of Z boson
    double mass_peak_width = 2.5; // Width of the mass peak

    TH1F *histogram = new TH1F("histogram", "Z Boson Mass Peak", 100, 80, 100);

    // ROOT's TRandom3 for random number generation
    TRandom3 random_generator;

    for (int i = 0; i < nEvents; ++i) {
        double mass = random_generator.Gaus(mass_peak_mean, mass_peak_width);
        histogram->Fill(mass);
    }

    // Draw histogram
    histogram->Draw();
}

double z_boson_cross_section(double mass) {
    // Realistic leading order cross section formula (Breit-Wigner distribution)
    double mass_width = 2.5;  // Width of the Z boson mass peak
    double resonance_mass = 91.2;  // Mass of the Z boson

    double term1 = 1.0 / (2.0 * 3.14159 * mass_width);
    double term2 = pow((mass_width / 2.0), 2);
    double term3 = pow((mass - resonance_mass), 2) + pow((mass_width / 2.0), 2);

    return term1 / term3;
}

// hit or miss

void generate_z_peak_hit_or_miss_cpp() {
    // Generating Z boson mass peak using hit-or-miss method
    const int nEvents = 10000;
    double mass_peak_min = 80.0;
    double mass_peak_max = 100.0;

    TH1F *histogram = new TH1F("histogram", "Z Boson Mass Peak", 100, mass_peak_min, mass_peak_max);

    // ROOT's TRandom3 for random number generation
    TRandom3 random_generator;

    for (int i = 0; i < nEvents; ++i) {
        // Generate random mass within the specified range
        double random_mass = random_generator.Uniform(mass_peak_min, mass_peak_max);

        // Generate a random number between 0 and 1 for hit-or-miss comparison
        double random_number = random_generator.Uniform(0.0, 1.0);

        // Evaluate the cross section at the generated mass
        double cross_section = z_boson_cross_section(random_mass);

        // Accept or reject based on hit-or-miss comparison
        if (random_number < cross_section) {
            histogram->Fill(random_mass);
        }
    }

    // Draw histogram
    histogram->Draw();
}
