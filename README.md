Small tutorials with the aim of introducing someone in the HEP common mindset. First exercises focus on some ROOT conecpts, as an excuse to comment some basic knowledge (uncertainties, fits, MC, generators, etc.). The tutorial is assuming some python and C++ knowledge (dedicated tutorial are everywhere). More specific tutorial (e.g. statistics or HPC) are released separetly but maybe some new exercises will be added here too.

A preliminary setup step is needed (will be documented soon).

The repo contains snippets for each exercise but it's not complete: discussions are needed for each topic.

### Exercise 1: Introduction to ROOT in Python
#### Objective:
- Understand why ROOT is used in High-Energy Physics.
- Learn to create a TGraph from a columnar text file.
- Plotting graphs with errors.
- **Learn how to use the ROOT class references**

#### Steps:
1. **Introduction to ROOT in Python**
   - Brief overview of ROOT and its significance in High-Energy Physics.

2. **Creating a TGraph from a Columnar Text File**
   - Importing necessary ROOT modules in Python.
   - Reading a columnar text file and creating a TGraph.
   - Adding errors to the graph points.

3. **Plotting Graphs**
   - Using TGraph to plot the data.
   - Customizing the plot with labels, titles, colors and legends.

Hint snippet [here](graph.py).

#### Discussion items:
   - meaning of error bars
   - asymmetric uncertainty 
   - plot style

### Exercise 2: Introduction to Histograms in C++
#### Objective:
- Understand the concept and importance of histograms.
- Generate a Z boson mass peak using standard C++ random number generation.
- Compare with ROOT's random number generation.

#### Steps:
1. **Introduction to Histograms**
   - Explanation of histograms and their role in data analysis.
   - Why histograms are useful in High-Energy Physics.

2. **Generating Z Boson Mass Peak in C++**
   - Using standard C++ random number generation to mimic a Z boson mass peak.
   - Creating and filling a histogram with the generated data.

3. **ROOT Random Number Generation**
   - Introduction to ROOT's random number generation classes.
   - Generating Z boson mass peak using ROOT's random number generation.
   - Comparing the two approaches.

Hint snippets [here](histogram.cxx).

#### Discussion items:
   - concept of "events" (i.e. collisions) in HEP
   - poissonian uncertainties
   - computing efficiency 

### Exercise 3: TTree and Ntuples in C++ and Python
#### Objective:
- Understand the concept of TTree and why Ntuples are used.
- Use TGenPhaseSpace to simulate Z boson production and decay.
- Save the four-momenta in a TTree, both in C++ and Python.

#### Steps:
1. **Introduction to TTree and Ntuples**
   - Explanation of TTree and the significance of Ntuples.
   - Brief overview of TGenPhaseSpace for simulating particle decays.

2. **Simulating Z Boson Production and Decay in C++**
   - Using TGenPhaseSpace to simulate Z boson production and decay in C++.
   - Saving the four-momenta in a TTree.

3. **Simulating Z Boson Production and Decay in Python**
   - Performing the same simulation in Python using ROOT.
   - Discussing pointers and performance considerations in Python.

4. **Inspecting the TTree**
   - Manually inspecting the TTree to understand the structure of the data.

Hint snippet [here](tree.cxx) (C++ only). Python version left to the reader.

#### Discussion items:
   - try to save to txt file first
   - storage
   - tabular analysis and vectorial branches

### Exercise 4: Kinematics and Statistical Uncertainty
#### Objective:
- Study the kinematics of the generated data from the TTree.
- Introduce the concept of weights and statistical uncertainty in histograms and MC generation.

#### Steps:
1. **Studying Kinematics with the Generated Ntuple**
   - Extracting information from the TTree to study kinematic properties.
   - Producing relevant plots.

2. **Introduction to Weights and Statistical Uncertainty**
   - Explanation of weights in histograms and their significance.
   - Discussing statistical uncertainty in Monte Carlo (MC) generation.

3. **Applying Weights and Analyzing Uncertainty**
   - Applying weights to the histograms.
   - Discussing how statistical uncertainty affects the analysis.

Hint snippet [here](kinematics.cxx)

#### Discussion items:
   - why do we care about MC and their statistics
   - event generation and simulation
