# Introduction to ROOT in Python

# Import necessary ROOT modules
import ROOT

# Create a TGraph from a columnar text file
def create_tgraph_from_txt(filename):
    data = ROOT.TGraph(filename)
    return data

# Plot the TGraph
def plot_tgraph(data):
    canvas = ROOT.TCanvas("canvas", "TGraph Example", 800, 600)
    canvas.Draw()
    graph.SetMarkerStyle(ROOT.kCircle)
    graph.SetFillColor(ROOT.kBlack)
    data.Draw("APE")
    canvas.SetGrid()
    canvas.Update()
    
    

# Example usage
filename = "data.txt"
graph_data = create_tgraph_from_txt(filename)
plot_tgraph(graph_data)
