// Exercise 3: TTree and Ntuples in C++

#include <TTree.h>
#include <TGenPhaseSpace.h>

void simulate_z_decay_cpp() {
    // Simulating Z boson production and decay in C++
    TTree *tree = new TTree("tree", "Z Boson Decay");

    TLorentzVector initial_state(0, 0, 0, 7000); // Initial state at LHC energy
    TGenPhaseSpace event_generator;
    event_generator.SetDecay(initial_state, 2, new double[]{91.2, 0}); // Z boson mass

    double px, py, pz, E;

    tree->Branch("px", &px, "px/D");
    tree->Branch("py", &py, "py/D");
    tree->Branch("pz", &pz, "pz/D");
    tree->Branch("E", &E, "E/D");

    for (int i = 0; i < 10000; ++i) {
        event_generator.Generate();
        px = event_generator.GetDecay(0)->Px();
        py = event_generator.GetDecay(0)->Py();
        pz = event_generator.GetDecay(0)->Pz();
        E = event_generator.GetDecay(0)->E();
        tree->Fill();
    }

    // Manual inspection of TTree
    tree->Print();
}

double realistic_cross_section(double mass) {
    // Your realistic cross-section formula here
    // Example: a simple Breit-Wigner distribution
    double resonance_mass = 91.2;
    double width = 2.5;
    return 1.0 / (2.0 * 3.14159 * width) / (pow(mass - resonance_mass, 2) + pow(0.5 * width, 2));
}

void simulate_z_decay_realistic_cpp() {
    // Simulating Z boson production and decay with realistic cross-section dependence
    TTree *tree = new TTree("tree", "Z Boson Decay");

    TLorentzVector initial_state(0, 0, 0, 7000); // Initial state at LHC energy
    TGenPhaseSpace event_generator;
    event_generator.SetDecay(initial_state, 2, new double[]{91.2, 0}); // Z boson mass

    double px, py, pz, E;

    tree->Branch("px", &px, "px/D");
    tree->Branch("py", &py, "py/D");
    tree->Branch("pz", &pz, "pz/D");
    tree->Branch("E", &E, "E/D");

    // ROOT's TRandom3 for random number generation
    TRandom3 random_generator;

    for (int i = 0; i < 10000; ++i) {
        // Generate a random mass within a specified range
        double random_mass = random_generator.Uniform(80.0, 100.0);

        // Evaluate the cross section at the generated mass
        double cross_section = realistic_cross_section(random_mass);

        // Generate a random number between 0 and 1 for hit-or-miss comparison
        double random_number = random_generator.Uniform(0.0, 1.0);

        // Accept or reject based on hit-or-miss comparison
        if (random_number < cross_section) {
            // If accepted, proceed with the decay simulation
            event_generator.SetDecay(initial_state, 2, new double[]{random_mass, 0});
            event_generator.Generate();

            px = event_generator.GetDecay(0)->Px();
            py = event_generator.GetDecay(0)->Py();
            pz = event_generator.GetDecay(0)->Pz();
            E = event_generator.GetDecay(0)->E();

            tree->Fill();
        }
    }

    // Manual inspection of TTree
    tree->Print();
}

