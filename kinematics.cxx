// Kinematics and Statistical Uncertainty

// Assuming the TTree has been generated and filled in a previous step

void study_kinematics_cpp(TTree *tree) {
    // Studying kinematics with the generated TTree
    double px, py, pz, E;
    tree->SetBranchAddress("px", &px);
    tree->SetBranchAddress("py", &py);
    tree->SetBranchAddress("pz", &pz);
    tree->SetBranchAddress("E", &E);

    // Create histograms for kinematic variables
    TH1F *histo_px = new TH1F("histo_px", "Particle X-Momentum", 100, -200, 200);
    TH1F *histo_py = new TH1F("histo_py", "Particle Y-Momentum", 100, -200, 200);
    TH1F *histo_pz = new TH1F("histo_pz", "Particle Z-Momentum", 100, 0, 7000);
    TH1F *histo_E = new TH1F("histo_E", "Particle Energy", 100, 0, 7000);

    for (int i = 0; i < tree->GetEntries(); ++i) {
        tree->GetEntry(i);
        histo_px->Fill(px);
        histo_py->Fill(py);
        histo_pz->Fill(pz);
        histo_E->Fill(E);
    }

    // Draw histograms
    // (Code for drawing histograms)
}

void study_with_weights_cpp(TTree *tree) {
    // Studying kinematics with the generated TTree and applying weights
    double px, py, pz, E;
    tree->SetBranchAddress("px", &px);
    tree->SetBranchAddress("py", &py);
    tree->SetBranchAddress("pz", &pz);
    tree->SetBranchAddress("E", &E);

    // Create histograms for kinematic variables with weights
    TH1F *histo_px_weighted = new TH1F("histo_px_weighted", "Particle X-Momentum (Weighted)", 100, -200, 200);
    TH1F *histo_py_weighted = new TH1F("histo_py_weighted", "Particle Y-Momentum (Weighted)", 100, -200, 200);
    TH1F *histo_pz_weighted = new TH1F("histo_pz_weighted", "Particle Z-Momentum (Weighted)", 100, 0, 7000);
    TH1F *histo_E_weighted = new TH1F("histo_E_weighted", "Particle Energy (Weighted)", 100, 0, 7000);

    // Important: Specify Sumw2() to compute the sum of squares of weights
    histo_px_weighted->Sumw2();
    histo_py_weighted->Sumw2();
    histo_pz_weighted->Sumw2();
    histo_E_weighted->Sumw2();

    // Assuming you have weights calculated and stored in a variable 'weights'
    // You might need to calculate weights based on some criteria or correction factors

    for (int i = 0; i < tree->GetEntries(); ++i) {
        tree->GetEntry(i);
        double weight = calculate_weight_based_on_some_criteria(px, py, pz, E);
        
        histo_px_weighted->Fill(px, weight);
        histo_py_weighted->Fill(py, weight);
        histo_pz_weighted->Fill(pz, weight);
        histo_E_weighted->Fill(E, weight);
    }

    // Draw histograms
    // (Code for drawing histograms)
}
